import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {
  applicants: any;
  loading: true;
  charactersJson: string;
  error: string;
  lol = { "wow": "great" };

  constructor(private http: HttpClient) {
    this.error = '';
  }

  ngOnInit() {
  }
  /**
  {"region": "EU"},
  {"groupType":"dungeon"},
  {"character": "Firemist","server": "Draenor","playerType": "applicant","role": "DAMAGER"},
  {"character": "Bilefroth","server": "Draenor","playerType": "self","role": "DAMAGER"}
   */
  getData(json) {
    this.http.put('/api/characters', json).subscribe((res) => {
      console.log('res', res);
      this.applicants = res;
    }, (err) => {
      console.log('err', err);
    });
  }

  pasteEvent(e: any) {
    this.applicants = [];
    const data = e.clipboardData.getData('text');
    console.log('pasteevent: ', data);
    if (this.checkIfJson(data)) {
      this.error = '';
      this.getData(this.convertToArray(data));
    } else {
      this.error = 'Pasted data is not a valid json.';
    }
  }

  convertToArray(obj) {
    var test = obj.split('\n');
    var arr = [];
    test.forEach((obj) => {
      var parsed = JSON.parse(obj.replace(/},/g, '}'));
      if (parsed.character) {
        arr.push(parsed);
      }
    });
    return arr;
  }

  checkIfJson(str) {
    return (typeof str === 'string') ? (/^[\],:{}\s]*$/.test(str.replace(/\\["\\\/bfnrtu]/g, '@').
      replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
      replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) : false;
  }
}
