import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Subject } from 'rxjs/Subject';
import { debounceTime, distinctUntilChanged, takeUntil, take, tap } from 'rxjs/operators';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnDestroy, OnInit {
  @Input() filterPlaceholder: string;
  filter: FormControl = new FormControl();
  updateFilter: (pattern: any) => void;

  private onDestroy = new Subject();

  ngOnInit() {
    this.filter.valueChanges
      .pipe(takeUntil(this.onDestroy), debounceTime(300), distinctUntilChanged())
      .subscribe(pattern => this.updateFilter(pattern));
  }

  ngOnDestroy() {
    this.onDestroy.next(true);
  }
}
