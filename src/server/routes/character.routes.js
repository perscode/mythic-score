const express = require('express');
const router = express.Router();
const http = require('http');
const services = require('../services');
const characterService = services.characterService;

router.put('/characters', (req, res) => {
  console.log('req.body', req.body);
  var list = req.body;
  var count = list.length;
  var done = false;
  var responseArray = [];
  if (list.length > 0 ) {
    list.forEach((character) => {
      characterService.getMythicScore(character, function(err, data) {
        if(err) {
          return res.status(500).json(err);
        } else if (responseArray.length < count) {
          responseArray.push(data);
          done = responseArray.length === count;
          console.log("got something back...", "\nresponseArray.length is:", responseArray.length, "\n count is:", count);
        }
        if (done) {
          responseArray.sort(compare);
          
          res.status(200).json(responseArray);
        }
      });
    });
  }
});

function compare(a,b) {
  if (a.rating.score < a.rating.score)
    return -1;
  if (a.rating.score > b.rating.score)
    return 1;
  return 0;
}

module.exports = router;
