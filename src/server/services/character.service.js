const htmlparser = require("htmlparser2");

function getMythicScore(character, callback) {
  var https = require('https');
  var url = "https://www.wowprogress.com/character/eu/" + character.server + "/" + character.character;
  var request = https.request(url, function (res) {
      var data = '';
      res.on('data', function (chunk) {
        data += chunk;
      });
      res.on('end', function () {
        return parseHtml(data, callback);
      });
      res.on('error', function (e) {
        console.log(e.message);
        callback(e);
    });
  });
  request.end(); 
}

function parseHtml(data, callback) {
  var gearScoreFound = false;
  var characterFound = false;
  var versionFound = false;
  var versionSaved = false;
  var recordClass = false;
  var recordRating = false;
  var recordName = false;
  var roleSaved = false;
  var chunk = "";
  var role = "";
  var resObj = {
    name: "",
    class: "",
    rating: []
  };
  var rating = {patch: "",score: "",role: ""};
  var parser = new htmlparser.Parser({
      onopentag: function(name, attribs) {
          if (name === 'div' && attribs.style === 'order:0;') {
            characterFound = true;
          }
          if (characterFound && name === 'h1') {
            recordName = true;
          }
          if (characterFound && name === 'i') {
            recordName = false;
            recordClass = true;
          }
          if (name === 'div' && attribs.class === 'gearscore') {
            gearScoreFound = true;
          }
          if(gearScoreFound && name === "span" && attribs.class === "small_tag info") {
            versionFound = true;
            // console.log("attribs", attribs);
          }
      },
      ontext: function(text) {
        if (gearScoreFound) {
          if (!roleSaved && text === 'Mythic+ Score DPS ') {
            console.log("found role DPS");
            role = "dps";
            roleSaved = true;
          } else if (!roleSaved && text === "Mythic+ Score Tank ") {
            console.log("found role Tank");
            role = "tank";
            roleSaved = true;
          } else if (!roleSaved && text === "Mythic+ Score ") {
            console.log("found role Normal");
            role = "normal";
            roleSaved = true;
          }
        }
        if (recordName && text) {
          resObj.name = resObj.name + text;
        }
        if (recordClass && text) {
          resObj.class = resObj.class + text;
        }
        if (versionFound && text === "7.3.2" || text === "7.3.0") {
          rating.patch = text;
          versionSaved = true;
        }
        if (recordRating) {
          rating.score = rating.score + text;
        }
      },
      onclosetag: function(tagname) {
        if (characterFound && tagname === 'h1') {
          recordName = false;
        }
        if (characterFound && tagname === 'i') {
          characterFound = recordClass = false;
        }
        if (versionSaved && tagname === 'span') {
          recordRating = true;
        }
        if (recordRating && tagname === 'div') {
          recordRating = false;
        }
        if(tagname === "div" && versionSaved) {
          roleSaved = false;
          gearScoreFound = versionSaved = versionFound = false;
          rating.role = role;
          rating.score = parseInt(rating.score.replace(/: /g, ''));
          resObj.rating.push(rating);
          rating = {patch: "",score: "",role: ""};
          // var decimalIndex = chunk.substr(chunk.indexOf('.'));
        }
      }
  }, {decodeEntities: true});
  
  parser.write(data);
  parser.end();
  callback(null, resObj);
}


module.exports = {
  parseHtml,
  getMythicScore
};
