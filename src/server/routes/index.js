const express = require('express');
const router = express.Router();

router.use('/', require('./character.routes'));

module.exports = router;
